# Django-API
Assignment created for Wings ICT Solutions - LIAROU Eleni

## Prerequisites

- Active python3 installation
- MySQL Server Installation

## Deployment

In order to perform CRUD operations first we need to deploy our application.

- We install in a virtual env of Python3 (named wingsproject)  with command

``` python3 -m venv env ``` 

the requirements given on the requirements.txt file by running

``` pip install -r requirements.txt ```

- We have a MySQL Db with credentials and naming the ones given in api/settings.py file in the DATABASES list. So first we need to migrate to use this DB. 

``` python manage.py migrate ```

- We can see by running in the terminal  ``` python manage.py runserver ```  we get the address to the API.

## Create dummy data

- ``` python -m pip install randomtimestamp ``` to install randomtimestamp

- ``` python dummydata.py ``` to populate the DB

## How it works ?
The urls to follow are described to the first page of the API and also here:

## BASIC DRUD OPERATIOSN
# To create devices
http://127.0.0.1:8000/api/device/ 

# To delete or update a device object
http://127.0.0.1:8000/api/device/device_id 

# To view all mesaurements
http://127.0.0.1:8000/api/measurement/all

# To create a mesaurement
http://127.0.0.1:8000/api/measurement/

# To get a specific measurement object
http://127.0.0.1:8000/api/measurement/measurement_id 

## FEATURES
- To return a device according to the serial number navigate to the 
http://127.0.0.1:8000/api/device/ and add the query parameter 'serial' equal to the one of the device you want to be returned. 

- To get all devices that measured above a specific threshold in a given interval of time we navigate to the URL /api/filter/threshold/
and add the query parameter ?threshold=threshold&start=start&stop=stop where start and stop define the time range. Important is to give the start and stop parameters as DD-MM-YYYY 00:00:00 (24h)
i.e. http://127.0.0.1:8000/api/filter/threshold/?threshold=42&start=05-01-2021%2002:00:00&stop=05-01-2021%2005:00:00

- To get the average measure for a specific device in a specific date range we navigate to the /api/filter/dates/  and add the query parameters ?serial_number=serial_number&start=start&stop=stop as before
i.e. http://127.0.0.1:8000/api/filter/dates/?serial=OJAHBBLFR&start=01-01-2021%2000:00:00&stop=19-04-2021%2000:00:00

## IN CASE OF AN ERROR RAISED SAYING THE API MODULE DOES NOT EXIST RUN 
``` pip install api ```