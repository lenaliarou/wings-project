import os, django, random ,string

from randomtimestamp import randomtimestamp

from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'level0.settings'
application = get_wsgi_application()

from level0.api.models import Device, Measurement
from django.utils import timezone

def create_devices():
    for i in range(10):
        latitude = random.uniform(50.9,200.3)
        longitude = random.uniform(50.9,200.3)
        serial_number = ''.join(random.choices(string.ascii_uppercase, k=9))
        name = ''.join(random.choices(string.ascii_uppercase, k=5))
        functional_status = random.choice([True, False])
        device = Device(latitude=latitude, longitude=longitude, serial_number=serial_number, name=name, functional_status=functional_status)
        device.save()

create_devices()

def create_measurements():
    for i in range(20):
        CO = random.uniform(10.1,200.5)
        PM25 = random.uniform(25.0,100.1)
        PM10 = random.uniform(25.0,100.1)
        N02 = random.uniform(25.0,100.1)
        temperature = random.uniform(0.0,50.0)
        humidity = random.uniform(0.0,60.0)
        S02 = random.uniform(10.0,100.0)
        timestamps = randomtimestamp(start_year=2021, text=True)
        data = {"CO":CO, "PM25":PM25, "PM10": PM10, "N02":N02, "temperature" : temperature, "humidity": humidity, "S02" :S02, "timestamp": timestamps}
        device_ids = Device.objects.all()
        random_item = random.choice(device_ids)       
        measurement = Measurement(device=random_item, data=data)
        measurement.save()

create_measurements()