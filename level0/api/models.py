# Create your models here.
from django.db import models
from faker import Faker
import random

class Device(models.Model):
    latitude = models.FloatField(max_length=10)
    longitude = models.FloatField(max_length=10)
    serial_number = models.CharField(max_length=15)
    name = models.CharField(max_length=15)
    functional_status = models.BooleanField()

    class Meta:
        app_label = 'api'



class Measurement(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    data = models.JSONField()

    class Meta:
        app_label = 'api'

