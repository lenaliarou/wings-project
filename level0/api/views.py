
from datetime import datetime, timedelta

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import generics

from .models import Device
from .models import Measurement
from .serializers import DeviceSerializer, MeasurementSerializer

from django.db.models import Q
from django.db.models import Avg

import os, django
from django.core.wsgi import get_wsgi_application
os.environ['DJANGO_SETTINGS_MODULE'] = 'level0.settings'
application = get_wsgi_application()
from level0.api.models import Device, Measurement


@api_view(['GET', 'POST'])
def device_list(request):
    if request.method == 'GET':
        data = Device.objects.all()
        #To filter according to serial number
        serial = request.GET.get('serial',None)
        if serial:
            data = data.filter(serial_number=serial)
       
        serializer = DeviceSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DeviceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT', 'DELETE'])
def device_detail(request, pk):
    try:
        device = Device.objects.get(pk=pk)
    except Device.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        serializer = DeviceSerializer(device, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        device.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def measurement_one(request,pk):
    if request.method == 'GET':
        data = Measurement.objects.get(pk=pk)

        serializer = MeasurementSerializer(data, context={'request': request})#,many=True)

        return Response(serializer.data)

@api_view(['GET'])
def measurement_list(request):
    if request.method == 'GET':
        data = Measurement.objects.all()

        serializer = MeasurementSerializer(data, context={'request': request},many=True)

        return Response(serializer.data)

@api_view(['POST'])
def measurement_create(request):
    data = Measurement.objects.all()
    serializer = MeasurementSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
            
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def filter_device_threshold(request):
    '''
        Implements the functionality of filtering the measurements according to a threshold and time interval and return the devices.
    '''
    mylist =[]
    if request.method == 'GET':

        threshold = request.GET.get('threshold',None)
        timestamp_from = request.GET.get('start',None)
        timestamp_to = request.GET.get('stop',None)

        threshold = float(threshold)

        query = Q(data__timestamp__range=[timestamp_from, timestamp_to]) & Q(data__N02__gt=threshold)
        measures = Measurement.objects.filter(query)
        
        for i in range(len(measures)):
            if measures[i].device not in mylist:
                mylist.append(measures[i].device)

    serializer = DeviceSerializer(mylist, context={'request': request}, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def filter_measurement_dates(request):
    ''' 
        Implements the funtionality of returning average measurements for a given device in a 
        defined date interval. 
    '''
    if request.method == 'GET':
        data = Device.objects.all()
        serial = request.GET.get('serial',None)
        if serial:
            data1 = data.filter(serial_number=serial)
            measures = Measurement.objects.filter(device = data1[0].id)

            #Timestamp _from and _to should be of the following format 01-01-2021 09:00:00
            timestamp_from = request.GET.get('start',None)
            timestamp_to = request.GET.get('stop',None)
            measures = measures.filter(data__timestamp__range=[timestamp_from, timestamp_to])
           
            # To return average measures and not all the measures. The idea here is to go and take each of the Json Dict values, average them 
            # and then return them in a new Json Dict corresponding to 'data'. Having this and the device id we can create a new Measurement object
            # which is the average one for the time interval. 
            data_of_the_dict = find_average(measures)
            device = Device.objects.get(id = data1[0].id)
            average_measures = create_measure(device, data_of_the_dict)   
            new_average_measure = Measurement.objects.filter(id = average_measures.id)

    serializer = MeasurementSerializer(new_average_measure, context={'request': request}, many=True)
    return Response(serializer.data)


# To create a new measurement object
def create_measure(device,data):
    measurement = Measurement(device=device, data=data)
    measurement.save()
    return measurement

#TO BE OPTIMIZED
# Returns the average 'data' dict
def find_average(query):
    mylist = list(query)
    data_C0_list = []
    data_N02_list = []
    data_S02_list = []
    data_PM10_list = []
    data_PM25_list = []
    data_humidity_list = []
    data_temperature_list = []
    for i in range(len(mylist)):
        data_C0_list.append(mylist[i].data["CO"])
        data_N02_list.append(mylist[i].data["N02"])
        data_S02_list.append(mylist[i].data["S02"])
        data_PM10_list.append(mylist[i].data["PM10"])
        data_PM25_list.append(mylist[i].data["PM25"])
        data_humidity_list.append(mylist[i].data["humidity"])
        data_temperature_list.append(mylist[i].data["temperature"])
    avg_CO = Average(data_C0_list)
    avg_N02 = Average(data_N02_list)
    avg_S02 = Average(data_S02_list)
    avg_PM10 = Average (data_PM10_list)
    avg_PM25 = Average(data_PM25_list)
    avg_humidity = Average(data_humidity_list)
    avg_temp = Average(data_temperature_list)
    return {"CO": avg_CO, "N02": avg_N02, "S02":avg_S02, "PM": avg_PM10, "PM25": avg_PM25, "humidity": avg_humidity, "temperature": avg_temp}


# Average of a list 
def Average(lst):
    return sum(lst)/len(lst)
